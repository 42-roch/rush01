/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   generator.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/12 15:44:12 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/12 15:47:38 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_check_matrix(int *input, int *m);
void	ft_update_values(int *matrix, int index, int i);

int	ft_run(int *input, int *matrix)
{
	int	c;
	int	d;

	d = 0;
	c = 0;
	while (c <= 24)
	{
		ft_update_values(matrix, 8, c);
		d = 0;
		while (d <= 24)
		{
			ft_update_values(matrix, 12, d);
			if (ft_check_matrix(input, matrix) == 16)
				return (1);
			d++;
		}
		c++;
	}
	return (0);
}

int	ft_generate_matrix(int *input, int *matrix)
{
	int	a;
	int	b;

	a = 0;
	while (a <= 24)
	{
		b = 0;
		ft_update_values(matrix, 0, a);
		while (b <= 24)
		{
			ft_update_values(matrix, 4, b);
			if (ft_run(input, matrix) == 1)
				return (1);
			b++;
		}
		a++;
	}
	return (0);
}

int	ft_set_values_1(int *v, int i, int b)
{
	if (i > 12 && i <= 16)
	{
		b = i % 4;
		v[0] = 1;
		v[1] = 2;
		v[2] = 4;
		v[3] = 3;
	}
	else if (i > 16 && i <= 20)
	{
		b = i % 4;
		v[0] = 1;
		v[1] = 3;
		v[2] = 2;
		v[3] = 4;
	}
	else
	{
		b = i % 4;
		v[0] = 1;
		v[1] = 4;
		v[2] = 3;
		v[3] = 2;
	}
	return (b);
}

int	ft_set_values(int *v, int i, int b)
{
	if (i <= 4)
	{
		b = i;
		v[0] = 1;
		v[1] = 2;
		v[2] = 3;
		v[3] = 4;
	}
	else if (i > 4 && i <= 8)
	{
		b = i % 4;
		v[0] = 1;
		v[1] = 3;
		v[2] = 4;
		v[3] = 2;
	}
	else if (i > 8 && i <= 12)
	{
		b = i % 4;
		v[0] = 1;
		v[1] = 4;
		v[2] = 2;
		v[3] = 3;
	}
	return (b);
}

void	ft_update_values(int *matrix, int index, int i)
{
	int	a;
	int	b;
	int	t[4];
	int	c;

	c = 0;
	a = 0;
	b = 0;
	b += ft_set_values(t, i, b);
	if (b == 0)
		b += ft_set_values_1(t, i, b);
	while (a < 4)
	{
		c = 0;
		c = t[a] + b;
		if (c > 4)
			c = c - 4;
		matrix[a + index] = c;
		a++;
	}
}
