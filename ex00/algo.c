/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/12 11:53:17 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/12 13:52:41 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_check_for_duplicates(int *m, int *check)
{
	int	a;

	a = 0;
	while (a < 4)
	{
		if (m[0 + a] == m[4 + a]
			|| m[4 + a] == m[8 + a]
			|| m[8 + a] == m[12 + a]
			|| m[0 + a] == m[8 + a]
			|| m[4 + a] == m[12 + a]
			|| m[0 + a] == m[12 + a])
			check = 0;
		if (m[0 + a] + m[4 + a] + m[8 + a] + m[12 + a] != 10)
			check = 0;
		a++;
	}
}

int	ft_check_line(int index, int a, int b, int c)
{
	int	d;

	d = 10 - (a + b + c);
	if (index == 1 && a == 4)
		return (1);
	if (index == 2
		&& (b == 4
			|| (a > b && c == 4)
			|| (a > b && a > c && d == 4)))
		return (1);
	if (index == 3
		&& ((c == 4 && a < b)
			|| (c == 3 && a == 2 && b == 1)
			|| (d == 4 && a < b && b > a)
			|| (d == 4 && a > b && c > a)))
		return (1);
	if (index == 4
		&& a == 1
		&& b == 2
		&& c == 3)
		return (1);
	return (0);
}

void	ft_check_matrix_1(int *in, int i, int *m, int *c)
{
	int	l;
	int	a;

	while (i <= 11 && i >= 8)
	{
		l = m[(i - 8) * 4];
		a = m[((i - 8) * 4) + 1];
		*c += ft_check_line(in[i], l, a, m[((i - 8) * 4) + 2]);
		i++;
	}
	while (i <= 15 && i >= 12)
	{
		l = m[((i - 12) * 4) + 3];
		a = m[((i - 12) * 4) + 2];
		*c += ft_check_line(in[i], l, a, m[((i - 12) * 4) + 1]);
		i++;
	}
}

int	ft_check_matrix(int *input, int *m)
{
	int	check;
	int	i;
	int	fin;

	check = 0;
	fin = 0;
	i = 0;
	while (i <= 3)
	{
		check += ft_check_line(input[i], m[i], m[i + 4], m[i + 8]);
		i++;
	}
	while (i <= 7 && i >= 4)
	{
		check += ft_check_line(input[i], m[i + 8], m[i + 4], m[i]);
		i++;
	}
	ft_check_matrix_1(input, i, m, &check);
	ft_check_for_duplicates(m, &check);
	return (check);
}
