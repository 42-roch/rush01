/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printer.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/12 12:20:19 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/12 15:51:21 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_print_matrix(int *matrix)
{
	int		i;
	int		y;
	char	temp;
	char	ligne;
	char	space;

	i = 0;
	ligne = '\n';
	space = ' ';
	while (i < 16)
	{
		y = 0;
		while (y < 4)
		{
			temp = matrix[i] + '0';
			write(1, &temp, 1);
			if ((i + 1) % 4 != 0)
				write(1, &space, 1);
			i++;
			y++;
		}
		write(1, &ligne, 1);
	}
}

int	ft_print_error(void)
{
	char	ligne;

	ligne = '\n';
	write(2, "Error", 6);
	write(1, &ligne, 1);
	return (0);
}
