/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_manager.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/12 13:44:56 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/12 14:04:20 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	ft_strlen(char *str);

int	ft_validate_arguments(char *str)
{
	int	i;
	int	len;

	len = ft_strlen(str);
	i = 0;
	while (i < len)
	{
		if (str[i] < '1' || str[i] > '4')
			return (0);
		i += 2;
	}
	i = 1;
	while (i < len)
	{
		if (str[i] != ' ')
			return (0);
		i += 2;
	}
	if (str[len - 1] < '1' || str[len - 1] > '4')
		return (0);
	return (1);
}

int	*ft_process_arguments(char *str)
{
	int	*input;
	int	i;
	int	a;
	int	len;

	i = 0;
	a = 0;
	len = ft_strlen(str);
	input = malloc(len * sizeof(*input) + 4);
	i = 0;
	while (str[i] && str[i] < '9' && str[i] > '0')
	{
		input[a] = str[i] - '0';
		i += 2;
		a++;
	}
	return (input);
}
