/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/11 12:42:04 by ptrancha          #+#    #+#             */
/*   Updated: 2021/09/12 15:51:19 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_process_arguments(char *str);
int		ft_sqrt(int nb);
int		ft_check_matrix(int *input);
int		*ft_generate_matrix(int *input, int *matrix);
void	ft_print_matrix(int *matrix);
void	ft_print_error(void);
int		ft_validate_arguments(char *str);
int		*ft_process_arguments(char *str);

int	main(int argc, char **argv)
{
	int	*input;
	int	*matrix;

	if (argc > 2 || argc < 2 || ft_validate_arguments(argv[1]) == 0)
		return (ft_print_error());
	input = ft_process_arguments(argv[1]);
	matrix = malloc(16 * sizeof(int) + 4);
	if (ft_generate_matrix(input, matrix) == 0)
		ft_print_error();
	else
		ft_print_matrix(matrix);
	free(matrix);
	free(input);
}
